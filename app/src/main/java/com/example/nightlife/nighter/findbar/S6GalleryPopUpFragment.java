package com.example.nightlife.nighter.findbar;

import android.app.Activity;
import android.app.Fragment;
import android.app.ListFragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.nightlife.nighter.Bar;
import com.example.nightlife.nighter.Globals;
import com.example.nightlife.nighter.R;
import com.example.nightlife.nighter.util.MyUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sept on 2015/09/30.
 */
public final class S6GalleryPopUpFragment extends Fragment {

    private static final String KEY_BAR = "bar";


    public static S6GalleryPopUpFragment newInstance(final Bar bar) {
        final S6GalleryPopUpFragment f = new S6GalleryPopUpFragment();

        // Supply index input as an argument.
        final Bundle args = new Bundle();
        args.putParcelable(KEY_BAR, bar);
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Bundle arguments = getArguments();
        final Bar bar = arguments.getParcelable(KEY_BAR);

        final View view = inflater.inflate(R.layout.s6_fragment_tag_it_pop_up, container, false);
        view.findViewById(R.id.button_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "onClick", Toast.LENGTH_SHORT).show();
                getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
            }
        });

        view.findViewById(R.id.imageView6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchChooser();
            }
        });

        final Handler handler = new Handler();

        view.findViewById(R.id.sendImageView).setOnClickListener(new View.OnClickListener() {
                                                                     @Override
                                                                     public void onClick(View v) {
                                                                         new Thread(new Runnable() {
                                                                             @Override
                                                                             public void run() {
                                                                                 handler.post(new Runnable() {
                                                                                     @Override
                                                                                     public void run() {
                                                                                         Toast.makeText(getActivity(), "Sending", Toast.LENGTH_SHORT).show();
                                                                                     }
                                                                                 });


                                                                                 boolean b = false;
                                                                                 if (uri != null) {
                                                                                     try {
                                                                                         final Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                                                                                         System.out.println("bitmap != null = " + (bitmap != null));
                                                                                         Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap, 800, 600, false);
                                                                                         bitmap.recycle();
                                                                                         System.out.println("bitmap2 != null = " + (bitmap2 != null));
                                                                                         b = MyUtils.sendPicture(3166, Globals.INSTANCE.getUuid(), bitmap2);
                                                                                         bitmap2.recycle();

                                                                                     } catch (IOException e) {
                                                                                         e.printStackTrace();
                                                                                     }
                                                                                 }
                                                                                 if (b) {
                                                                                     handler.post(new Runnable() {
                                                                                         @Override
                                                                                         public void run() {
                                                                                             Toast.makeText(getActivity(), "Success!", Toast.LENGTH_SHORT).show();
                                                                                             System.out.println("Success");
                                                                                         }
                                                                                     });
                                                                                 } else {
                                                                                     handler.post(new Runnable() {
                                                                                         @Override
                                                                                         public void run() {
                                                                                             Toast.makeText(getActivity(), "Dont success", Toast.LENGTH_SHORT).show();
                                                                                             System.out.println("Dont Success");
                                                                                         }
                                                                                     });
                                                                                 }
                                                                             }
                                                                         }).start();
                                                                     }
                                                                 }

        );

        final GridView gridView = (GridView) view.findViewById(R.id.gridView);
        final MyGridViewAdapter adapter = new MyGridViewAdapter(getActivity());
        gridView.setAdapter(adapter);

        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<Bitmap> bitmaps = MyUtils.getPictures(3166, 10, 0);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.addAll(bitmaps);
                    }
                });
            }
        }).start();


        return view;

    }

    private Uri mPictureUri;
    private Uri uri;

    private void launchChooser() {
        // ギャラリーから選択
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.setType("image/*");
        i.addCategory(Intent.CATEGORY_OPENABLE);

        // カメラで撮影
        String filename = System.currentTimeMillis() + ".jpg";
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, filename);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        mPictureUri = getActivity().getContentResolver()
                .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent i2 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        i2.putExtra(MediaStore.EXTRA_OUTPUT, mPictureUri);

        // ギャラリー選択のIntentでcreateChooser()
        Intent chooserIntent = Intent.createChooser(i, "Pick Image");
        // EXTRA_INITIAL_INTENTS にカメラ撮影のIntentを追加
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{i2});

        startActivityForResult(chooserIntent, IMAGE_CHOOSER_RESULTCODE);
    }

    private static final int IMAGE_CHOOSER_RESULTCODE = 0x8e84;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_CHOOSER_RESULTCODE) {

            if (resultCode != Activity.RESULT_OK) {
                if (mPictureUri != null) {
                    getActivity().getContentResolver().delete(mPictureUri, null, null);
                    mPictureUri = null;
                }
                return;
            }

            // 画像を取得
            Uri result = (data == null) ? mPictureUri : data.getData();

            uri = mPictureUri;

            mPictureUri = null;
        }
    }
}

class MyGridViewAdapter extends ArrayAdapter<Bitmap> {
    public MyGridViewAdapter(Context context) {
        super(context, 0, 0, new ArrayList<Bitmap>());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = new ImageView(getContext());
        }
        ImageView imageView = (ImageView) convertView;

        imageView.setImageBitmap(getItem(position));

        return convertView;
    }
}