package com.example.nightlife.nighter.findbar;

import android.app.Fragment;
import android.app.ListFragment;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nightlife.nighter.Bar;
import com.example.nightlife.nighter.R;
import com.example.nightlife.nighter.view.ImageTextView;

import java.util.ArrayList;

/**
 * Created by sept on 2015/09/30.
 */
public final class S4BarExtraInfoFragment extends ListFragment {

    private static final String KEY_BAR = "bar";

    public static S4BarExtraInfoFragment newInstance(final Bar bar) {
        final S4BarExtraInfoFragment f = new S4BarExtraInfoFragment();

        // Supply index input as an argument.
        final Bundle args = new Bundle();
        args.putParcelable(KEY_BAR, bar);
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Bundle arguments = getArguments();
        final Bar bar = arguments.getParcelable(KEY_BAR);

        final View view = inflater.inflate(R.layout.s4_latest_fragment_bar_extra_info_rating_header, container, false);

        view.findViewById(R.id.view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.fragment_container, S5TagItPopUpFragment.newInstance(null))
                        .addToBackStack(null)
                        .commit();
            }
        });

        view.findViewById(R.id.view2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.fragment_container, S6GalleryPopUpFragment.newInstance(null))
                        .addToBackStack(null)
                        .commit();
            }
        });

        ((ImageTextView)view.findViewById(R.id.title_bar)).textView.setText(bar.name);

        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        final ListView listView = getListView();
        listView.addHeaderView(LayoutInflater.from(getActivity()).inflate(R.layout.s4_list_view_header, null, false), null, false);
        ArrayAdapter arrayAdapter = new ArrayAdapter<Object>(getActivity(),R.layout.s4_latest_list_view_item){
            @Override
        public View getView(int position, View convertView, ViewGroup parent){
                View view;

                if (convertView == null) {
                    view = View.inflate(getActivity(), R.layout.s4_latest_list_view_item, null);
                } else {
                    view = convertView;
                }

                return view;
            }
        };
        arrayAdapter.add("1");
        arrayAdapter.add("2");
        arrayAdapter.add("3");
        arrayAdapter.add("4");
        arrayAdapter.add("5");
        arrayAdapter.add("6");
        arrayAdapter.add("7");
        listView.setAdapter(arrayAdapter);
    }
}
