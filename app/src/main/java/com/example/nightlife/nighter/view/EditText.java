package com.example.nightlife.nighter.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

/**
 * Created by sept on 2015/10/16.
 */
// Eras Font EditText
public final class EditText extends android.widget.EditText {

    public EditText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        setTypeface(Typeface.createFromAsset(context.getAssets(), "erasbd.ttf"));
        // Don't close asset!! That's why app become to be not able to run.
        // asset.close();
    }
}
