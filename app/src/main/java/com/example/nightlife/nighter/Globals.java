package com.example.nightlife.nighter;


import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.nightlife.nighter.util.MySynchronizedList;
import com.example.nightlife.nighter.view.EditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

/**
 * Created by sept on 2015/10/29.
 */
// Singleton
// This class is immutable. Bar[] is mutable.
// If use UnmodifiableList<Bar>, I need to reload the data from database too slowly.
public enum Globals {
    INSTANCE;

    private static final String KEY_UUID = "uuid";

    private String uuid;

    // this is public and mutable! Be careful to use!
    @Deprecated
    public MySynchronizedList<Bar> bars;

    // public MutableBar[] mutableBars;
    // private MutableBar[] mutableBarsForSort;

    public double latitude = Double.NaN;
    public double longitude = Double.NaN;

    public void initialize(Context context) {
        this.uuid = getUuidOrCreate(context);

        // Don't forget defensive copying!
        final Bar[] barArray = MySQLiteOpenHelper.readFastest(context);
        bars = new MySynchronizedList<>(barArray);

        // mutableBars = MySQLiteOpenHelper.readFastestAsMutableBar(context);
        // mutableBarsForSort = MutableBar.shallowCopy(mutableBars);
        // Collections.sort(bars, comparator);
    }

    public void sortByDistance(final double latitude, final double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        bars.sort(new MyComparator(latitude, longitude));
        // Arrays.sort(mutableBars, new MutableBarComparator(latitude, longitude));
    }

    public final String getUuid() {
        return uuid;
    }

    static class MyComparator implements Comparator<Bar> {
        final float[] lhsResult = new float[1];
        final float[] rhsResult = new float[1];

        final double latitude;
        final double longitude;

        public MyComparator(final double latitude, final double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Override
        public int compare(Bar lhs, Bar rhs) {
            if (lhs.latitude == 0 && lhs.longitude == 0 || rhs.latitude == 0 && rhs.latitude == 0)
                return Integer.MAX_VALUE;

            Location.distanceBetween(latitude, longitude, lhs.latitude, lhs.longitude, lhsResult);
            Location.distanceBetween(latitude, longitude, rhs.latitude, rhs.longitude, rhsResult);
            return (int) (lhsResult[0] - rhsResult[0]);
        }
    }

    static class MutableBarComparator implements Comparator<MutableBar> {
        final float[] lhsResult = new float[1];
        final float[] rhsResult = new float[1];

        final double latitude;
        final double longitude;

        public MutableBarComparator(final double latitude, final double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Override
        public int compare(MutableBar lhs, MutableBar rhs) {
            if (lhs.latitude == 0 && lhs.longitude == 0 || rhs.latitude == 0 && rhs.latitude == 0)
                return Integer.MAX_VALUE;

            Location.distanceBetween(latitude, longitude, lhs.latitude, lhs.longitude, lhsResult);
            Location.distanceBetween(latitude, longitude, rhs.latitude, rhs.longitude, rhsResult);
            return (int) (lhsResult[0] - rhsResult[0]);
        }
    }

    private static final String getUuidOrCreate(final Context context) {
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        String uuid = sharedPreferences.getString(KEY_UUID, null);

        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
            sharedPreferences.edit().putString(KEY_UUID, uuid).commit();
        }

        return uuid;
    }

    // TODO to use type
    @Deprecated
    public final List<Bar> findBarTheMostClicked(final String type, final int number) {
        //Arrays.sort(mutableBarsForSort, new Comparator<MutableBar>() {
        //    @Override
        //   public int compare(MutableBar lhs, MutableBar rhs) {
        //        return (int) (rhs.clickCount - lhs.clickCount);
        //    }
        //});
        bars.sort(new Comparator<Bar>() {
            @Override
            public int compare(Bar lhs, Bar rhs) {
                return (int) (rhs.clickCount - lhs.clickCount);
            }
        });
        return bars.subList(0, number);
    }

    private static <T> int binarySearchById(MutableBar[] bars, long id) {
        int lo = 0;
        int hi = bars.length - 1;

        while (lo <= hi) {
            int mid = (lo + hi) >>> 1;
            int midValCmp = (int) (bars[mid].id - id);

            if (midValCmp < 0) {
                lo = mid + 1;
            } else if (midValCmp > 0) {
                hi = mid - 1;
            } else {
                return mid;  // value found
            }
        }
        return ~lo;  // value not present
    }

    private static <T> void sortById(MutableBar[] bars) {
        Arrays.sort(bars, new Comparator<MutableBar>() {
            @Override
            public int compare(MutableBar lhs, MutableBar rhs) {
                return (int) (lhs.id - rhs.id);
            }
        });
    }


    public final Bar findBarById(final long id) {
        //return binarySearchById(mutableBarsForSort,id);


        final int size = bars.size();
        for (int i = 0; i < size; i++) {
            final Bar bar = bars.get(i);
            if (bar.id == id)
                return bar;
        }
        return null;
    }

    public final Bar setBar(final long id, final Bar bar) {
        final int size = bars.size();
        for (int i = 0; i < size; i++) {
            final Bar b = bars.get(i);
            if (b.id == id)
                return bars.set(i, bar);
        }
        return null;
    }

}