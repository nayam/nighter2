package com.example.nightlife.nighter.util;

import android.support.annotation.NonNull;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by sept on 2015/11/04.
 */
// Probably this is faster than Collections#synchronizedList. I'll test this performance.
public final class MySynchronizedList<E> implements List<E> {

    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    private final E[] a;

    public MySynchronizedList(E[] storage) {
        if (storage == null) {
            throw new NullPointerException("storage == null");
        }
        a = storage;
    }

    public MySynchronizedList(final List<E> storage) {
        if (storage == null) {
            throw new NullPointerException("storage == null");
        }
        a = (E[]) storage.toArray();
    }

    @Override
    public void add(int location, E object) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(E object) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int location, Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean contains(Object object) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public E get(int location) {
        readWriteLock.readLock().lock();
        try {
            return a[location];
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    @Override
    public int indexOf(Object object) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException();
    }


    // required by for-each
    @NonNull
    @Override
    public Iterator<E> iterator() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int lastIndexOf(Object object) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<E> listIterator() {
        return listIterator(0);
    }

    @NonNull
    @Override
    public ListIterator<E> listIterator(final int location) {
        return new ListIterator<E>() {

            int lastPosition = -1;
            int pos;

            {
                if (location >= 0 && location <= size()) {
                    pos = location - 1;
                } else {
                    throw new IndexOutOfBoundsException();
                }
            }


            @Override
            public void add(E object) {
                throw new UnsupportedOperationException();
            }

            @Override
            public boolean hasNext() {
                return pos + 1 < size();
            }

            @Override
            public boolean hasPrevious() {
                throw new UnsupportedOperationException();
            }

            @Override
            public E next() {
                try {
                    final E result = get(pos + 1);
                    lastPosition = ++pos;
                    return result;
                } catch (IndexOutOfBoundsException e) {
                    throw new NoSuchElementException();
                }
            }

            @Override
            public int nextIndex() {
                throw new UnsupportedOperationException();
            }

            @Override
            public E previous() {
                throw new UnsupportedOperationException();
            }

            @Override
            public int previousIndex() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void set(E object) {
                try {
                    MySynchronizedList.this.set(lastPosition, object);
                } catch (IndexOutOfBoundsException e) {
                    throw new IllegalStateException();
                }
            }
        };
    }

    @Override
    public E remove(int location) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean remove(Object object) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public E set(int location, E object) {
        readWriteLock.writeLock().lock();
        try {
            E result = a[location];
            a[location] = object;
            return result;
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    @Override
    public int size() {
        readWriteLock.readLock().lock();
        try {
            return a.length;
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    @NonNull
    @Override
    public List<E> subList(int start, int end) {
        if (start >= 0 && end <= size()) {
            if (start <= end) {
                final E[] dst = (E[]) new Object[end - start];
                System.arraycopy(a, start, dst, 0, end - start);
                return new MySynchronizedList<>(dst);
            }
            throw new IllegalArgumentException();
        }
        throw new IndexOutOfBoundsException();
    }

    @NonNull
    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }


    // Collections#sort requires this method.
    @NonNull
    @Override
    public <T> T[] toArray(T[] array) {
        readWriteLock.readLock().lock();
        try {
            final int size = size();
            final int length = array.length;
            if (size > length)
                array = (T[]) new Object[size];

            System.arraycopy(a, 0, array, 0, size);

            for (int s = size; s < length; s++) {
                array[s] = null;
            }
            return array;
        } finally {
            readWriteLock.readLock().unlock();
        }
    }

    public void sort(Comparator<? super E> comparator){
        readWriteLock.writeLock().lock();
        try {
            Arrays.sort(a, comparator);
        }finally {
            readWriteLock.writeLock().unlock();
        }
    }
}
