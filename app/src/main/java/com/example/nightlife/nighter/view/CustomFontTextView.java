package com.example.nightlife.nighter.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.nightlife.nighter.R;

/**
 * Created by sept on 2015/10/16.
 */
// Eras Font EditText
public final class CustomFontTextView extends TextView {

    public CustomFontTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        final TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomFontTextView,
                0, 0);

        try {
            final String string = a.getString(R.styleable.CustomFontTextView_font);
            if (string != null)
                setTypeface(Typeface.createFromAsset(context.getAssets(), string));
        } finally {
            a.recycle();
        }

        // Don't close asset!! That's why app become to be not able to run.
        // asset.close();
    }
}
