package com.example.nightlife.nighter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.example.nightlife.nighter.util.MyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sept on 2015/09/30.
 */
public class MySQLiteOpenHelper extends SQLiteOpenHelper {


    private static final String COLUMN_NAME_ID = "_id";
    private static final String COLUMN_NAME_NAME = "name";
    private static final String COLUMN_NAME_ADDRESS = "address";
    private static final String COLUMN_NAME_LATITUDE = "latitude";
    private static final String COLUMN_NAME_LONGITUDE = "longitude";
    private static final String COLUMN_NAME_POST_ADDRESS = "post_address";
    private static final String COLUMN_NAME_TYPE = "type";
    private static final String COLUMN_NAME_AVERAGE = "average";
    private static final String COLUMN_NAME_RATINGS = "ratings";
    private static final String COLUMN_NAME_CLICK_COUNT = "click_count";


    private static final String COLUMN_NAME_MY_FAVORITE = "my_favorite";
    private static final String COLUMN_NAME_MY_RATING = "my_rating";

    private static final String TABLE_NAME_MY_RATINGS = "my_ratings";
    private static final String TABLE_NAME_BARS = "my_table";

    private static final String SQL_CREATE_BARS =
            "CREATE TABLE " + TABLE_NAME_BARS + " (" +
                    COLUMN_NAME_ID + " INTEGER PRIMARY KEY," +
                    COLUMN_NAME_NAME + " TEXT," +
                    COLUMN_NAME_ADDRESS + " TEXT," +
                    COLUMN_NAME_LATITUDE + " REAL," +
                    COLUMN_NAME_LONGITUDE + " REAL," +
                    COLUMN_NAME_POST_ADDRESS + " TEXT," +
                    COLUMN_NAME_TYPE + " TEXT," +
                    COLUMN_NAME_AVERAGE + " INTEGER," +
                    COLUMN_NAME_RATINGS + " INTEGER," +
                    COLUMN_NAME_CLICK_COUNT + " INTEGER)";

    private static final String SQL_CREATE_MY_RATINGS =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_MY_RATINGS + " (" +
                    COLUMN_NAME_ID + " INTEGER PRIMARY KEY," +
                    COLUMN_NAME_MY_FAVORITE + " INTEGER," +
                    COLUMN_NAME_MY_RATING + " REAL)";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + TABLE_NAME_BARS;
    public static final int DATABASE_VERSION = 46;
    public static final String DATABASE_NAME = "MyDatabase.db";

    private static final String SQL_SELECT_ALL_FROM_TABLES = "SELECT * FROM " +
            TABLE_NAME_BARS + " NATURAL LEFT OUTER JOIN " + TABLE_NAME_MY_RATINGS;

    // when update, we cannot use limit! Nov. 4th
    private static final String SQL_UPDATE_RATING_WHERE_ID = "UPDATE " + TABLE_NAME_MY_RATINGS +
            " SET " + COLUMN_NAME_MY_RATING + "=?" +
            " WHERE " +
            COLUMN_NAME_ID + "=?";

    private static final String SQL_UPDATE_FAVORITE_WHERE_ID = "UPDATE " + TABLE_NAME_MY_RATINGS +
            " SET " + COLUMN_NAME_MY_FAVORITE + "=?" +
            " WHERE " +
            COLUMN_NAME_ID + "=?";

    private MySQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_BARS);
        addFromTomiinsDatabase(db);

        db.execSQL(SQL_CREATE_MY_RATINGS);
    }

    private final void addFromTomiinsDatabase(SQLiteDatabase db) {
        final JSONArray jsonArray = MyUtils.getLocationsOnThread();
        db.beginTransaction();
        try {
            SQLiteStatement stmt = db.compileStatement("INSERT INTO " + TABLE_NAME_BARS + " VALUES (?,?,?,?,?,?,?,?,?,?);");

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    final JSONObject jsonObject = jsonArray.getJSONObject(i);
                    final long id = jsonObject.getLong("id");
                    final String name = jsonObject.getString("name");
                    final String address = jsonObject.getString("address");
                    final double lat = jsonObject.getDouble("lat");
                    final double lng = jsonObject.getDouble("lng");
                    final String postaddress = jsonObject.getString("postaddress");

                    final JSONArray types = jsonObject.getJSONArray("type");
                    final int length = types.length();

                    final StringBuilder stringBuilder = new StringBuilder();
                    for (int index = 0; index < length; index++)
                        stringBuilder.append(types.getString(index)).append(',');
                    stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                    final String type = stringBuilder.toString();

                    final JSONObject rating = jsonObject.getJSONObject("rating");
                    final double average = rating.getDouble("average");
                    final long ratings = rating.getLong("ratings");

                    final long clickCount = jsonObject.getLong("clicks");

                    stmt.bindLong(1, id);
                    stmt.bindString(2, name);
                    stmt.bindString(3, address);
                    stmt.bindDouble(4, lat);
                    stmt.bindDouble(5, lng);
                    stmt.bindString(6, postaddress);
                    stmt.bindString(7, type);
                    stmt.bindDouble(8, average);
                    stmt.bindLong(9, ratings);
                    stmt.bindLong(10, clickCount);
                    stmt.executeInsert();
                } catch (JSONException jsonException) {
                    jsonException.printStackTrace();
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }


    /* I want to use UnmodifiableList. But array is faster than List. 4th Nov.*/
    public static Bar[] readFastest(Context context) {
        /* Database を読み取り専用で開く */
        final SQLiteDatabase db = new MySQLiteOpenHelper(context).getReadableDatabase();

        try {
            final Cursor cursor = db.rawQuery(SQL_SELECT_ALL_FROM_TABLES, null);
            try {
                final int idIndex = cursor.getColumnIndex(COLUMN_NAME_ID);
                final int nameIndex = cursor.getColumnIndex(COLUMN_NAME_NAME);
                final int addressIndex = cursor.getColumnIndex(COLUMN_NAME_ADDRESS);
                final int latitudeIndex = cursor.getColumnIndex(COLUMN_NAME_LATITUDE);
                final int longitudeIndex = cursor.getColumnIndex(COLUMN_NAME_LONGITUDE);
                final int postAddressIndex = cursor.getColumnIndex(COLUMN_NAME_POST_ADDRESS);
                final int typeIndex = cursor.getColumnIndex(COLUMN_NAME_TYPE);
                final int averageIndex = cursor.getColumnIndex(COLUMN_NAME_AVERAGE);
                final int ratingsIndex = cursor.getColumnIndex(COLUMN_NAME_RATINGS);
                final int clicksIndex = cursor.getColumnIndex(COLUMN_NAME_CLICK_COUNT);
                final int myFavoriteIndex = cursor.getColumnIndex(COLUMN_NAME_MY_FAVORITE);
                final int myRatingIndex = cursor.getColumnIndex(COLUMN_NAME_MY_RATING);

                final Bar[] bars = new Bar[cursor.getCount()];

                while (cursor.moveToNext())
                    bars[cursor.getPosition()] = new Bar(cursor.getLong(idIndex),
                            cursor.getString(nameIndex),
                            cursor.getString(addressIndex),
                            cursor.getDouble(latitudeIndex),
                            cursor.getDouble(longitudeIndex),
                            cursor.getString(postAddressIndex),
                            cursor.getString(typeIndex),
                            cursor.getDouble(averageIndex),
                            cursor.getLong(ratingsIndex),
                            cursor.isNull(myFavoriteIndex) ? false : cursor.getLong(myFavoriteIndex) != 0,
                            cursor.isNull(myRatingIndex) ? Float.NaN : cursor.getFloat(myRatingIndex),
                            cursor.getLong(clicksIndex));

                return bars;
            } finally {
                cursor.close();
            }
        } finally {
            db.close();
        }
    }

    /* I want to use UnmodifiableList. But array is faster than List. 4th Nov.*/
    public static final MutableBar[] readFastestAsMutableBar(final Context context) {
        /* Database を読み取り専用で開く */
        final SQLiteDatabase db = new MySQLiteOpenHelper(context).getReadableDatabase();

        try {
            final Cursor cursor = db.rawQuery(SQL_SELECT_ALL_FROM_TABLES, null);
            try {
                final int idIndex = cursor.getColumnIndex(COLUMN_NAME_ID);
                final int nameIndex = cursor.getColumnIndex(COLUMN_NAME_NAME);
                final int addressIndex = cursor.getColumnIndex(COLUMN_NAME_ADDRESS);
                final int latitudeIndex = cursor.getColumnIndex(COLUMN_NAME_LATITUDE);
                final int longitudeIndex = cursor.getColumnIndex(COLUMN_NAME_LONGITUDE);
                final int postAddressIndex = cursor.getColumnIndex(COLUMN_NAME_POST_ADDRESS);
                final int typeIndex = cursor.getColumnIndex(COLUMN_NAME_TYPE);
                final int averageIndex = cursor.getColumnIndex(COLUMN_NAME_AVERAGE);
                final int ratingsIndex = cursor.getColumnIndex(COLUMN_NAME_RATINGS);
                final int clicksIndex = cursor.getColumnIndex(COLUMN_NAME_CLICK_COUNT);
                final int myFavoriteIndex = cursor.getColumnIndex(COLUMN_NAME_MY_FAVORITE);
                final int myRatingIndex = cursor.getColumnIndex(COLUMN_NAME_MY_RATING);

                final MutableBar[] bars = new MutableBar[cursor.getCount()];

                while (cursor.moveToNext()) {
                    bars[cursor.getPosition()] = new MutableBar(
                            cursor.getLong(idIndex),
                            cursor.getString(nameIndex),
                            cursor.getString(addressIndex),
                            cursor.getDouble(latitudeIndex),
                            cursor.getDouble(longitudeIndex),
                            cursor.getString(postAddressIndex),
                            cursor.getString(typeIndex),
                            cursor.getDouble(averageIndex),
                            cursor.getLong(ratingsIndex),
                            cursor.isNull(myFavoriteIndex) ? false : cursor.getLong(myFavoriteIndex) != 0,
                            cursor.isNull(myRatingIndex) ? Float.NaN : cursor.getFloat(myRatingIndex),
                            cursor.getLong(clicksIndex));
                }
                return bars;
            } finally {
                cursor.close();
            }
        } finally {
            db.close();
        }
    }

    public static void updateRating(Context context, float rating, long id) {
        /* To get writable database */
        final SQLiteDatabase db = new MySQLiteOpenHelper(context).getWritableDatabase();

        db.beginTransaction();
        try {
            final SQLiteStatement statement = db.compileStatement(SQL_UPDATE_RATING_WHERE_ID);
            try {
                statement.bindDouble(1, rating);
                statement.bindLong(2, id);
                statement.executeUpdateDelete();
            } finally {
                statement.close();
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        db.close();
    }


    public static void updateFavorite(Context context, boolean favorite, long id) {
        /* To get writable database */
        final SQLiteDatabase db = new MySQLiteOpenHelper(context).getWritableDatabase();

        db.beginTransaction();
        try {
            final SQLiteStatement statement = db.compileStatement(SQL_UPDATE_FAVORITE_WHERE_ID);
            try {
                statement.bindLong(1, favorite ? 1 : 0);
                statement.bindLong(2, id);
                statement.executeUpdateDelete();
            } finally {
                statement.close();
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        db.close();
    }

}
