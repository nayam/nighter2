package com.example.nightlife.nighter;

import android.app.ListFragment;
import android.media.Image;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.example.nightlife.nighter.findbar.BarFragment;
import com.example.nightlife.nighter.findbar.FilterableAdapter;
import com.example.nightlife.nighter.findbar.FindBarFragment;
import com.example.nightlife.nighter.util.MyUtils;
import com.example.nightlife.nighter.view.ImageTextView;

import java.util.List;

/**
 * Created by sept on 2015/09/24.
 */
public final class S1Fragment extends ListFragment {

    private FilterableAdapter adapter;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        adapter = new FilterableAdapter(getActivity(), Globals.INSTANCE.bars);
        adapter.filterSetEmpty();
        getListView().setAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.latest_main_fragment, container, false);

        view.findViewById(R.id.buttonClubs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(0, R.animator.flip_left_out, 0, R.animator.flip_left_out)
                        .replace(R.id.fragment_container, FindBarFragment.newInstance("club"))
                        .addToBackStack(null)
                        .commit();
            }
        });

        view.findViewById(R.id.buttonPubs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.fragment_container, FindBarFragment.newInstance("pub"))
                        .addToBackStack(null)
                        .commit();
            }
        });

        view.findViewById(R.id.buttonSports).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.fragment_container, FindBarFragment.newInstance("sports"))
                        .addToBackStack(null)
                        .commit();
            }
        });

        view.findViewById(R.id.buttonLounge).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.fragment_container, FindBarFragment.newInstance("lounge"))
                        .addToBackStack(null)
                        .commit();
            }
        });

        final ImageTextView popularPub1 = (ImageTextView) view.findViewById(R.id.popularPub1);

        final List<Bar> popularBars = Globals.INSTANCE.findBarTheMostClicked("pub", 2);
        final Bar popularPub1Bar = popularBars.get(0);
        final Bar popularPub2Bar = popularBars.get(1);
        popularPub1.textView.setText(popularPub1Bar.name);
        popularPub1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyUtils.incrementClickCountOnThread(popularPub1Bar.id, Globals.INSTANCE.getUuid());
                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.fragment_container, BarFragment.newInstance(popularPub1Bar))
                        .addToBackStack(null)
                        .commit();
            }
        });


        final ImageTextView popularPub2 = (ImageTextView) view.findViewById(R.id.popularPub2);
        popularPub2.textView.setText(popularPub2Bar.name);
        popularPub2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyUtils.incrementClickCountOnThread(popularPub2Bar.id, Globals.INSTANCE.getUuid());
                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.fragment_container, BarFragment.newInstance(popularPub2Bar))
                        .addToBackStack(null)
                        .commit();
            }
        });

        final ImageTextView popularClub1 = (ImageTextView)view.findViewById(R.id.popularClub1);
        final ImageTextView popularClub2 = (ImageTextView)view.findViewById(R.id.popularClub2);

        final List<Bar> popularClubs = Globals.INSTANCE.findBarTheMostClicked("club", 2);
        final Bar popularClub1Bar = popularClubs.get(0);
        final Bar popularClub2Bar = popularClubs.get(1);
        popularClub1.textView.setText(popularClub1Bar.name);
        popularClub1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyUtils.incrementClickCountOnThread(popularClub1Bar.id, Globals.INSTANCE.getUuid());
                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.fragment_container, BarFragment.newInstance(popularClub1Bar))
                        .addToBackStack(null)
                        .commit();
            }
        });

        popularClub2.textView.setText(popularClub2Bar.name);
        popularClub2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyUtils.incrementClickCountOnThread(popularClub2Bar.id, Globals.INSTANCE.getUuid());
                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.fragment_container, BarFragment.newInstance(popularClub2Bar))
                        .addToBackStack(null)
                        .commit();
            }
        });

        view.findViewById(R.id.buttonLiveMusic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.fragment_container, FindBarFragment.newInstance("livemusic"))
                        .addToBackStack(null)
                        .commit();
            }
        });

        final EditText editText = (EditText) view.findViewById(R.id.editText);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("")) {
                    if (adapter != null) {
                        adapter.filterSetEmpty();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        view.findViewById(R.id.imageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String string = editText.getText().toString();
                if (!string.equals("")) {
                    if (adapter != null) {
                        adapter.filter(string, null);
                    }
                }
            }
        });



        return view;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // TODO It doesnt work on no connection environment
        //final boolean success = MyUtils.incrementClickCountOnThread(id, Globals.INSTANCE.getUuid());
        //Toast.makeText(getActivity(), String.valueOf(success), Toast.LENGTH_SHORT).show();

        getFragmentManager().beginTransaction()
                //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.fragment_container, BarFragment.newInstance(Globals.INSTANCE.findBarById(id)))
                .addToBackStack(null)
                .commit();
    }
}
