package com.example.nightlife.nighter.view;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nightlife.nighter.R;

/**
 * Created by sept on 2015/10/16.
 */
// A view which TextView on ImageView.
public final class ImageTextView extends FrameLayout {
    private static final ViewGroup.LayoutParams LAYOUT_PARAMS = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

    public final ImageView imageView;
    public final TextView textView;

    // When view is inflated from xml, 2 arguments function is called.
    // If I want to inflate from xml, I must add 2 arguments function!
    // Don't forget!!
    public ImageTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        imageView = new ImageView(context);
        textView = new TextView(context) {
            @Override
            public boolean isFocused() {
                return true;
            }
        };

        init(context, attrs);
    }

    private final void init(Context context, AttributeSet attrs) {

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MyImageTextView);

        /* textAppearance は 最初に 呼ばないと 色々上書きされる*/
        final int textAppearance = a.getResourceId(R.styleable.MyImageTextView_myTextAppearance, android.R.style.TextAppearance_Medium);
        final CharSequence text = a.getText(R.styleable.MyImageTextView_myText);
        final int color = a.getColor(R.styleable.MyImageTextView_myTextColor, Color.BLACK);
        final int resourceId = a.getResourceId(R.styleable.MyImageTextView_mySrc, 0);
        final String scaleType = a.getString(R.styleable.MyImageTextView_myScaleType);
        final boolean autoScroll = a.getBoolean(R.styleable.MyImageTextView_myAutoScroll, true);
        a.recycle();

        textView.setGravity(Gravity.CENTER);
        if (autoScroll) {
            textView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            textView.setSingleLine();
        }

        textView.setTextAppearance(context, textAppearance);

        textView.setText(text);

        textView.setTextColor(color);

        imageView.setImageResource(resourceId);

        if (scaleType != null) {
            String st = scaleType.replaceAll("(\\p{Lower})(\\p{Upper})", "$1_$2").toUpperCase();
            imageView.setScaleType(ImageView.ScaleType.valueOf(st.toUpperCase()));
        }


        /* To set the font */
        final AssetManager asset = context.getAssets();
        textView.setTypeface(Typeface.createFromAsset(asset, "erasbd.ttf"));
        // Don't close asset!! That's why app become to be not able to use other asset. -> Can't run.
        // asset.close();

        addView(imageView, LAYOUT_PARAMS);
        textView.setWidth(imageView.getWidth());
        addView(textView, LAYOUT_PARAMS);
    }

}
