package com.example.nightlife.nighter.findbar;


import android.app.ListFragment;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ListView;
import android.widget.Toast;

import com.example.nightlife.nighter.Globals;
import com.example.nightlife.nighter.R;
import com.example.nightlife.nighter.util.MyUtils;
import com.example.nightlife.nighter.view.EditText;
import com.example.nightlife.nighter.view.ImageTextView;
import com.google.android.gms.maps.LocationSource;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by sept on 2015/09/24.
 */

public final class FindBarFragment extends ListFragment {
    private static final String KEY_TYPE = "type";

    private FilterableAdapter filterableAdapter;
    private String type;

    public void onLocationChanged(final Location location) {
        filterableAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        type = getArguments().getString(KEY_TYPE);
        filterableAdapter = new FilterableAdapter(getActivity(), Globals.INSTANCE.bars);

        filterableAdapter.filter(null, type);
        setListAdapter(filterableAdapter);
    }

    public static FindBarFragment newInstance(final String type) {
        final FindBarFragment fragment = new FindBarFragment();

        final Bundle args = new Bundle();
        args.putString(KEY_TYPE, type);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Bundle arguments = getArguments();
        final String type = arguments.getString(KEY_TYPE);

        final View view = inflater.inflate(R.layout.fragment_find_bar_club, container, false);
        ((EditText) view.findViewById(R.id.editText)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // なぜか2回呼ばれる
                filterableAdapter.filter(s.toString(), type);
            }
        });

        final ImageTextView imageTextView = (ImageTextView) view.findViewById(R.id.title_bar);

        switch (type) {
            case "club":
            imageTextView.imageView.setImageResource(R.drawable.s2_bar_clubs_header);
                break;
            case "pub":
                imageTextView.imageView.setImageResource(R.drawable.s2_bar_pubs_header);
                break;
            default:
                break;
        }
        imageTextView.textView.setText(type.toUpperCase());
        return view;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // TODO it doesn't work on no-connection
        //final boolean success = MyUtils.incrementClickCountOnThread(id, Globals.INSTANCE.getUuid());
        // Toast.makeText(getActivity(), String.valueOf(success), Toast.LENGTH_SHORT).show();

        getFragmentManager().beginTransaction()
                //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.fragment_container, BarFragment.newInstance(Globals.INSTANCE.findBarById(id)))
                .addToBackStack(null)
                .commit();
    }

}