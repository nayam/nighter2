package com.example.nightlife.nighter;

import com.example.nightlife.nighter.util.MyUtils;

/**
 * Created by sept on 2015/09/15.
 */
public final class MutableBar {
    // Primitive or Immutable Object
    public final long id;
    public final String name;
    public final String address;
    public final double latitude;
    public final double longitude;
    public final String postAddress;
    public final String type;
    public final double average;
    public final long ratings;
    public final long clickCount;

    // Mutable. *Be careful!
    public boolean myFavorite;
    public float myRating;

    /* I can add builder pattern. But this one is faster. */
    public MutableBar(long id, String name, String address, double latitude, double longitude, String postAddress, String type,
                      double average, long ratings, boolean myFavorite, float myRating, long clickCount) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.postAddress = postAddress;
        this.type = type;
        this.average = average;
        this.ratings = ratings;
        this.myFavorite = myFavorite;
        this.myRating = myRating;
        this.clickCount = clickCount;
    }

    private MutableBar(MutableBar that) {
        this.id = that.id;
        this.name = that.name;
        this.address = that.address;
        this.latitude = that.latitude;
        this.longitude = that.longitude;
        this.postAddress = that.postAddress;
        this.type = that.type;
        this.average = that.average;
        this.ratings = that.ratings;
        this.myFavorite = that.myFavorite;
        this.myRating = that.myRating;
        this.clickCount = that.clickCount;
    }

    public void setMyRating(final float myRating) {
        if (myRating < 0 || myRating > 5)
            throw new IllegalArgumentException("Rating must be 0.0 to 5.0");

        this.myRating = myRating;
    }

    public void setMyFavorite(final boolean myFavorite) {
        this.myFavorite = myFavorite;
    }

    @Override
    public String toString() {
        return MyUtils.toString(this);
    }

    public static final MutableBar[] shallowCopy(final MutableBar[] src) {
        final int length = src.length;
        final MutableBar[] dst = new MutableBar[length];
        System.arraycopy(src, 0, dst, 0, length);
        return dst;
    }

    public static final MutableBar[] deepCopy(final MutableBar[] src) {
        final int length = src.length;
        final MutableBar[] dst = new MutableBar[length];
        // faster than for-each
        for (int i = 0; i < length; i++) {
            dst[i] = new MutableBar(src[i]);
        }
        return dst;
    }
}
