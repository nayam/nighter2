package com.example.nightlife.nighter.findbar;


import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;

import com.example.nightlife.nighter.Bar;
import com.example.nightlife.nighter.Globals;
import com.example.nightlife.nighter.R;
import com.example.nightlife.nighter.view.ImageTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by sept on 2015/10/03.
 */
public final class FilterableAdapter extends BaseAdapter {

    private final LayoutInflater layoutInflater;
    private final List<Bar> originalValues;
    private List<Bar> filteredValues;
    private String type;

    public FilterableAdapter(final Context context, final List<Bar> originalVaules) {
        layoutInflater = LayoutInflater.from(context);

        this.originalValues = originalVaules;
        this.filteredValues = originalVaules;

    }

    public void filterSetEmpty() {
        filteredValues = Collections.emptyList();

        this.notifyDataSetChanged();
    }

    // I know Filterable. but I use myFilter faster
    public void filter(@Nullable final String constraint, @Nullable final String type) {
        this.type = type;

        if (constraint == null && type == null) {
            filteredValues = originalValues;
        } else if (constraint == null && type != null) {
            filteredValues = Collections.unmodifiableList(new ArrayList<Bar>() {{
                for (int i = 0, size = originalValues.size(); i < size; i++) {
                    final Bar bar = originalValues.get(i);
                    if (bar.type.contains(type)) {
                        add(bar);
                    }
                }
            }});
        } else if (constraint != null && type == null) {
            final String lowerCase = constraint.toString().toLowerCase();
            filteredValues = Collections.unmodifiableList(new ArrayList<Bar>() {{
                for (int i = 0, size = originalValues.size(); i < size; i++) {
                    final Bar bar = originalValues.get(i);
                    if (bar.name.toLowerCase().contains(lowerCase)) {
                        add(bar);
                    }
                }
            }});
        } else {
            filteredValues = Collections.unmodifiableList(new ArrayList<Bar>() {{
                for (int i = 0, size = originalValues.size(); i < size; i++) {
                    final Bar bar = originalValues.get(i);
                    if (bar.name.toLowerCase().contains(constraint.toString().toLowerCase()) && bar.type.contains(type)) {
                        add(bar);
                    }
                }
            }});
        }


        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return filteredValues.size();
    }

    @Override
    public Bar getItem(int position) {
        return filteredValues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return filteredValues.get(position).id;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.new_bar_club_list_item, parent, false);
            holder = new ViewHolder();
            holder.nameView = (ImageTextView) convertView.findViewById(R.id.nameView);
            holder.ratingView = (ImageTextView) convertView.findViewById(R.id.ratingView);
            holder.distanceView = (ImageTextView) convertView.findViewById(R.id.distanceView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final boolean isLight = position % 2 == 0;

        switch (type) {
            case "club":
                if (isLight) {
                    holder.nameView.imageView.setImageResource(R.drawable.s2_bar_clubs_name_box_light);
                    holder.distanceView.imageView.setImageResource(R.drawable.s2_bar_clubs_rating_box_and_distance_light);
                    holder.ratingView.imageView.setImageResource(R.drawable.s2_bar_clubs_rating_box_and_distance_light);
                } else {
                    holder.nameView.imageView.setImageResource(R.drawable.s2_bar_clubs_name_box_dark);
                    holder.distanceView.imageView.setImageResource(R.drawable.s2_bar_clubs_rating_box_and_distance_dark);
                    holder.ratingView.imageView.setImageResource(R.drawable.s2_bar_clubs_rating_box_and_distance_dark);
                }
                break;
            case "pub":
                if (isLight) {
                    holder.nameView.imageView.setImageResource(R.drawable.s2_bar_pubs_name_box_light);
                    holder.distanceView.imageView.setImageResource(R.drawable.s2_bar_pubs_rating_box_and_distance_light);
                    holder.ratingView.imageView.setImageResource(R.drawable.s2_bar_pubs_rating_box_and_distance_light);
                } else {
                    holder.nameView.imageView.setImageResource(R.drawable.s2_bar_pubs_name_box_dark);
                    holder.distanceView.imageView.setImageResource(R.drawable.s2_bar_pubs_rating_box_and_distance_dark);
                    holder.ratingView.imageView.setImageResource(R.drawable.s2_bar_pubs_rating_box_and_distance_dark);
                }
                break;
            default:
                holder.nameView.imageView.setImageResource(R.drawable.s2_bar_name_green);
                holder.distanceView.imageView.setImageResource(R.drawable.s2_bar_distance_blue);
                holder.ratingView.imageView.setImageResource(R.drawable.s2_bar_rating_red);
                break;
        }

        final Bar bar = getItem(position);

        holder.nameView.textView.setText(bar.name);

        if (Float.isNaN(bar.myRating)) {
            holder.ratingView.textView.setText(new StringBuilder().append(String.format("%1.1f", bar.average)).append(" / 5"));
            holder.ratingView.textView.setTextColor(0xFFE3E3E3);
        } else {
            holder.ratingView.textView.setText(new StringBuilder().append(String.format("%1.1f", bar.myRating)).append(" / 5"));
            holder.ratingView.textView.setTextColor(Color.GREEN);
        }
        // もひとつのほうってなんでできないんだっけ？
        // fromUserがとれない

        //executorService.execute(new MyRunnable(bar.id));

        //if (Double.isNaN(lastLatitude) || Double.isNaN(lastLongitude) ||
        //        Double.isNaN(bar.latitude) || Double.isNaN(bar.longitude)) {
        //    holder.distanceView.textView.setText("Calculating");
        //} else {
        final Globals globals = Globals.INSTANCE;

        float[] results = new float[1];
        Location.distanceBetween(globals.latitude, globals.longitude, bar.latitude, bar.longitude, results);
        if (results[0] < 1000)
            holder.distanceView.textView.setText(String.format("%3.0fM", results[0]));
        else
            holder.distanceView.textView.setText(String.format("%2.1fKM", results[0] / 1000));
        //}

        return convertView;
    }

    static class ViewHolder {
        ImageTextView nameView;
        ImageTextView ratingView;
        ImageTextView distanceView;
    }
}