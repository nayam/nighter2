package com.example.nightlife.nighter.findbar;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.nightlife.nighter.Bar;
import com.example.nightlife.nighter.R;
import com.example.nightlife.nighter.view.ImageTextView;

/**
 * Created by sept on 2015/09/30.
 */
public final class S5TagItPopUpFragment extends ListFragment {

    private static final String KEY_BAR = "bar";

    public static S5TagItPopUpFragment newInstance(final Bar bar) {
        final S5TagItPopUpFragment f = new S5TagItPopUpFragment();

        // Supply index input as an argument.
        final Bundle args = new Bundle();
        args.putParcelable(KEY_BAR, bar);
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Bundle arguments =getArguments();
        arguments.getInt()

        final View view = inflater.inflate(R.layout.s5_fragment_tag_it_pop_up, container, false);

        view.findViewById(R.id.exitImageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
            }
        });

        ((ImageTextView)view.findViewById(R.id.title_bar)).textView.setText();

        //view.findViewById(R.id.button_back).setOnClickListener(new View.OnClickListener() {
        //   @Override
        //   public void onClick(View v) {
        //       Toast.makeText(getActivity(), "onClick", Toast.LENGTH_SHORT).show();
        //       getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
        //       getActivity().dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));
        //    }
        //});

        return view;

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        final ListView listView = getListView();
        //listView.addHeaderView(LayoutInflater.from(getActivity()).inflate(R.layout.s5, null, false), null, false);
        ArrayAdapter arrayAdapter = new ArrayAdapter<Object>(getActivity(), R.layout.s5_list_view_item) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view;

                if (convertView == null) {
                    view = View.inflate(getActivity(), R.layout.s5_list_view_item, null);
                } else {
                    view = convertView;
                }

                return view;
            }
        };
        arrayAdapter.add("1");
        arrayAdapter.add("2");
        arrayAdapter.add("3");
        arrayAdapter.add("4");
        arrayAdapter.add("5");
        arrayAdapter.add("6");
        arrayAdapter.add("7");
        listView.setAdapter(arrayAdapter);
    }
}
