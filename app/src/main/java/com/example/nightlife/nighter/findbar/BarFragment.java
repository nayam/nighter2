package com.example.nightlife.nighter.findbar;

import android.app.Fragment;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.nightlife.nighter.Bar;
import com.example.nightlife.nighter.Globals;
import com.example.nightlife.nighter.MainActivity;
import com.example.nightlife.nighter.MySQLiteOpenHelper;
import com.example.nightlife.nighter.R;
import com.example.nightlife.nighter.util.MyUtils;
import com.example.nightlife.nighter.view.ImageTextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by sept on 2015/09/30.
 */
public final class BarFragment extends Fragment {

    private static final String KEY_BAR = "bar";

    private static final Timer TIMER = new Timer();

    private MapView mapView;
    private GoogleMap googleMap;

    public static BarFragment newInstance(final Bar bar) {
        final BarFragment f = new BarFragment();

        // Supply index input as an argument.
        final Bundle args = new Bundle();
        args.putParcelable(KEY_BAR, bar);
        f.setArguments(args);

        return f;
    }

    private Thread threadRating;
    private Thread threadFavorite;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Bundle arguments = getArguments();
        final Bar bar = arguments.getParcelable(KEY_BAR);

        final View view = inflater.inflate(R.layout.s3_latest_fragment_bar, container, false);
        final ImageTextView nameTextView = (ImageTextView) view.findViewById(R.id.title_bar);
        final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        final CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkBox);
        final ImageTextView imageButton = (ImageTextView) view.findViewById(R.id.information);

        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        nameTextView.textView.setText(bar.name);

        final Context applicationContext = getActivity().getApplicationContext();

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(final RatingBar ratingBar, final float rating, final boolean fromUser) {
                if (fromUser) {
                    if (threadRating != null)
                        // Cancel old thread.
                        threadRating.interrupt();
                    threadRating = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (Thread.interrupted())
                                return;
                            MySQLiteOpenHelper.updateRating(applicationContext, rating, bar.id);
                            if (Thread.interrupted())
                                return;
                            Globals.INSTANCE.setBar(bar.id, bar.changeOnlyMyRating(rating));
                            if (Thread.interrupted())
                                return;
                            MyUtils.updateRating(bar.id, rating, Globals.INSTANCE.getUuid());
                        }
                    });
                    threadRating.start();

                    // After 5 seconds, interrupt it
                    TIMER.schedule(new MyUtils.InterruptTimerTask(threadRating), 5000);
                }
            }
        });

        ratingBar.setRating(bar.myRating);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                if (threadFavorite != null) {
                    // Cancel old thread.
                    threadFavorite.interrupt();
                }
                threadFavorite = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Globals.INSTANCE.setBar(bar.id, bar.changeOnlyMyFavorite(isChecked));
                        MySQLiteOpenHelper.updateFavorite(applicationContext, isChecked, bar.id);
                    }
                });
                threadFavorite.start();

                // After 5 seconds, interrupt it
                TIMER.schedule(new MyUtils.InterruptTimerTask(threadFavorite), 5000);
            }
        });

        checkBox.setChecked(bar.myFavorite);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction()
                        //.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.fragment_container, S4BarExtraInfoFragment.newInstance(bar))
                        .addToBackStack(null)
                        .commit();
            }
        });

        // Gets to GoogleMap from the MapView and does initialization stuff
        //googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        if ((googleMap = mapView.getMap()) != null) {
            googleMap.getUiSettings().setCompassEnabled(true);
            googleMap.setMyLocationEnabled(true);
            MapsInitializer.initialize(getActivity());

            final LatLng latLng = new LatLng(bar.latitude, bar.longitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
            googleMap.addMarker(new MarkerOptions().title(bar.name).position(latLng));
        }

        return view;

    }


    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

}