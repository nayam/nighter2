package com.example.nightlife.nighter;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.nightlife.nighter.findbar.FindBarFragment;

import java.util.List;
import java.util.UUID;

public final class MainActivity extends Activity {

    private static final long LOCATION_REFRESH_TIME = 0; // milli seconds
    private static final float LOCATION_REFRESH_DISTANCE = 0; // meter

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ステータスバー非表示
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        Globals.INSTANCE.initialize(getApplicationContext());

        final Location location = getLastKnownLocation(locationManager);
        if (location != null) {
            Globals.INSTANCE.sortByDistance(location.getLatitude(), location.getLongitude());
        }
        requestLocationUpdates();

        if (findViewById(R.id.fragment_container) != null) {

            if (savedInstanceState != null) {
                return;
            }

            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, new S1Fragment()).commit();
        }

    }


    private static final Location getLastKnownLocation(LocationManager locationManager) {
        final List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location location = null;
            try {
                location = locationManager.getLastKnownLocation(provider);
            } catch (SecurityException securityException) {
            }
            if (location == null) {
                continue;
            }
            if (bestLocation == null || location.getTime() < bestLocation.getTime()) {
                bestLocation = location;
            }
        }
        return bestLocation;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    LocationManager locationManager;

    @Override
    public void onStart() {
        super.onStart();
        requestLocationUpdates();
    }

    private static final int REQUEST_CODE = 1;

    public void requestLocationUpdates() {
        // TODO Need to search about api23
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            if (Build.VERSION.SDK_INT >= 23) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE);
            }
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_REFRESH_TIME,
                LOCATION_REFRESH_DISTANCE, locationListener);
    }

    private boolean finishFlag;

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0 || finishFlag) {
            super.onBackPressed();
        } else {
            Toast.makeText(this, "Press back once more", Toast.LENGTH_SHORT).show();
            finishFlag = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finishFlag = false;
                }
            }, 1000);
        }
    }


    public void onStop() {
        super.onStop();
        if (locationManager != null)
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        locationManager.removeUpdates(locationListener);
    }

    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            Toast.makeText(MainActivity.this, "onLocationChanged", Toast.LENGTH_SHORT).show();
            Globals.INSTANCE.sortByDistance(location.getLatitude(), location.getLongitude());
            final Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_container);
            if (fragment instanceof FindBarFragment) {
                ((FindBarFragment) fragment).onLocationChanged(location);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

}
