package com.example.nightlife.nighter;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.nightlife.nighter.util.MyUtils;

/**
 * Created by sept on 2015/09/15.
 */
// Immutable
public final class Bar implements Parcelable {
    // ONLY primitive or immutable
    public final long id;
    public final String name;
    public final String address;
    public final double latitude;
    public final double longitude;
    public final String postAddress;
    public final String type;
    public final double average;
    public final long ratings;
    public final boolean myFavorite;
    public final float myRating;
    public final long clickCount;

    /* I can add builder pattern. But this one is faster. */
    public Bar(long id, String name, String address, double latitude, double longitude, String postAddress, String type,
               double average, long ratings, boolean myFavorite, float myRating, long clickCount) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.postAddress = postAddress;
        this.type = type;
        this.average = average;
        this.ratings = ratings;
        this.myFavorite = myFavorite;
        this.myRating = myRating;
        this.clickCount = clickCount;
    }

    public Bar(final Bar that) {
        this.id = that.id;
        this.name = that.name;
        this.address = that.address;
        this.latitude = that.latitude;
        this.longitude = that.longitude;
        this.postAddress = that.postAddress;
        this.type = that.type;
        this.average = that.average;
        this.ratings = that.ratings;
        this.myFavorite = that.myFavorite;
        this.myRating = that.myRating;
        this.clickCount = that.clickCount;
    }

    private Bar(final Bar that, final float myRating) {
        this.id = that.id;
        this.name = that.name;
        this.address = that.address;
        this.latitude = that.latitude;
        this.longitude = that.longitude;
        this.postAddress = that.postAddress;
        this.type = that.type;
        this.average = that.average;
        this.ratings = that.ratings;
        this.myFavorite = that.myFavorite;
        this.myRating = myRating;
        this.clickCount = that.clickCount;
    }

    private Bar(final Bar that, final boolean myFavorite) {
        this.id = that.id;
        this.name = that.name;
        this.address = that.address;
        this.latitude = that.latitude;
        this.longitude = that.longitude;
        this.postAddress = that.postAddress;
        this.type = that.type;
        this.average = that.average;
        this.ratings = that.ratings;
        this.myFavorite = myFavorite;
        this.myRating = that.myRating;
        this.clickCount = that.clickCount;
    }


    public Bar changeOnlyMyRating(final float myRating) {
        return new Bar(this, myRating);
    }

    public Bar changeOnlyMyFavorite(final boolean myFavorite) {
        return new Bar(this, myFavorite);
    }

    @Override
    public String toString() {
        return MyUtils.toString(this);
    }

    public static final Bar[] shallowCopy(final Bar[] src) {
        final int length = src.length;
        final Bar[] dst = new Bar[length];
        System.arraycopy(src, 0, dst, 0, length);
        return dst;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(postAddress);
        dest.writeString(type);
        dest.writeDouble(average);
        dest.writeLong(ratings);
        dest.writeInt(myFavorite ? 1 : 0);
        dest.writeFloat(myRating);
        dest.writeLong(clickCount);
    }

    private Bar(Parcel in) {
        this.id  = in.readLong();
        this.name  = in.readString();
        this.address  = in.readString();
        this.latitude  = in.readDouble();
        this.longitude  = in.readDouble();
        this.postAddress  = in.readString();
        this.type  = in.readString();
        this.average  = in.readDouble();
        this.ratings  = in.readLong();
        this.myFavorite  = in.readInt() != 0;
        this.myRating  = in.readFloat();
        this.clickCount  = in.readLong();
    }

    public static final Parcelable.Creator<Bar> CREATOR
            = new Parcelable.Creator<Bar>() {
        @Override
        public Bar createFromParcel(Parcel in) {
            return new Bar(in);
        }

        @Override
        public Bar[] newArray(int size) {
            return new Bar[size];
        }
    };
}
