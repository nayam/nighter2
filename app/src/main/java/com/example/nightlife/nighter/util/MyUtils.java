package com.example.nightlife.nighter.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by sept on 2015/09/30.
 */
public enum MyUtils {
    ;

    public static final byte[] bitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream blob = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, blob);
        return blob.toByteArray();
    }

    public static final Bitmap byteArrayToBitmap(byte[] bitmap) {
        return BitmapFactory.decodeByteArray(bitmap, 0, bitmap.length);
    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public static final Bitmap getBitmapFromURL(String src) {
        HttpURLConnection connection = null;
        try {
            URL url = new URL(src);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        } finally {
            if (connection != null)
                connection.disconnect();

        }
    }

    public static final Address findAddressByName(Context context, String locationName) {
        Geocoder geocoder = new Geocoder(context);

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocationName(locationName, 1);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (IllegalArgumentException illegalArgumentException) {
            illegalArgumentException.printStackTrace();
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size() == 0) {
            return null;
        } else {
            return addresses.get(0);
        }
    }

    public static final JSONArray getLocations() {
        JSONArray jsonArray = null;
        HttpURLConnection httpURLConnection = null;
        try {
            URL url = new URL("http://users.metropolia.fi/~tomiin/nightlife/?api&getlocations&json");
            httpURLConnection = (HttpURLConnection) url.openConnection();
            final String str = InputStreamToString(httpURLConnection.getInputStream());
            jsonArray = new JSONArray(str);
        } catch (final Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }
        return jsonArray;
    }

    @Deprecated
    public static final JSONArray getLocationsOnThread() {
        final ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
        final Future<JSONArray> future = singleThreadExecutor.submit(new Callable<JSONArray>() {
            @Override
            public JSONArray call() {
                return getLocations();
            }
        });

        JSONArray jsonArray = null;
        try {
            jsonArray = future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        singleThreadExecutor.shutdownNow();
        return jsonArray;
    }


    public static final boolean updateLocation(long id, double lat, double lng) {
        HttpURLConnection httpURLConnection = null;
        try {
            final URL url = new URL("http://users.metropolia.fi/~tomiin/nightlife/?api&updatelocation&barid=" + id + "&lat=" + lat + "&lng=" + lng + "&json");
            httpURLConnection = (HttpURLConnection) url.openConnection();
            final String str = InputStreamToString(httpURLConnection.getInputStream());
            //final JSONArray jsonArray = new JSONArray(str);
            return Boolean.valueOf(str);
            //final Object object=jsonArray.get(0);

        } catch (final Exception ex) {
            return false;
        } finally {
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }
    }

    public static final boolean updateRating(final long barId, final float myRating, final String userId) {

        HttpURLConnection httpURLConnection = null;
        try {
            final URL url = new URL("http://users.metropolia.fi/~tomiin/nightlife/?api&updaterating&barid=" + barId + "&rating=" + myRating + "&userid=" + userId);

            httpURLConnection = (HttpURLConnection) url.openConnection();
            final String str = InputStreamToString(httpURLConnection.getInputStream());
            return str.contains("true");//Boolean.valueOf(str);
        } catch (final Exception ex) {
            return false;
        } finally {
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }
    }

    public static final boolean sendPicture(long id, String userid, Bitmap data) {
        HttpURLConnection httpURLConnection = null;
        try {
            // ?api&addimage&barid=???&username=???&data=??? optional &userid=???

            final URL url = new URL("http://users.metropolia.fi/~tomiin/nightlife/?api&addimage&barid=" + id + "&username=" + userid + "&json");
            System.out.println("http://users.metropolia.fi/~tomiin/nightlife/?api&addimage&barid=" + id + "&username=" + userid + "&json");

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");

            String urlParameters = "data=" + URLEncoder.encode(MyUtils.encodeTobase64(data), "UTF-8");//MyUtils.encodeTobase64(data);
            System.out.println("sending data!!!!" + urlParameters);
            //URLEncoder.encode("???", "UTF-8");
            con.setDoOutput(true);

            // con.setDoOutput(true);
            // DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + urlParameters);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            System.out.println(response.toString());


            // httpURLConnection = (HttpURLConnection) url.openConnection();
            // final String str = InputStreamToString(httpURLConnection.getInputStream());
            //final JSONArray jsonArray = new JSONArray(str);
            // return Boolean.valueOf(str);
            //final Object object=jsonArray.get(0);
            return true;

        } catch (final Exception ex) {
            return false;
        } finally {
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }
    }

    public static final Bitmap getPicture(long id) {
        HttpURLConnection httpURLConnection = null;
        try {
            final URL url = new URL("http://users.metropolia.fi/~tomiin/nightlife/?api&getimages&barid=" + id + "&limit=" + 1 + "&json");
            httpURLConnection = (HttpURLConnection) url.openConnection();
            final String str = InputStreamToString(httpURLConnection.getInputStream());
            System.out.println("str!!!!" + str);
            JSONArray jsonArray = new JSONArray(str);

            final JSONObject jsonObject = jsonArray.getJSONObject(0);
            final String data = jsonObject.getString("data");
            System.out.println("receive data!!!!" + data);

            //final JSONArray jsonArray = new JSONArray(str);
            return MyUtils.decodeBase64(data);
            //final Object object=jsonArray.get(0);

        } catch (final Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }
    }

    public static final List<Bitmap> getPictures(long id, int limit, int offset) {
        if (limit < 0)
            throw new IllegalArgumentException();
        if (offset < 0)
            throw new IllegalArgumentException();

        HttpURLConnection httpURLConnection = null;
        try {
            final URL url = new URL("http://users.metropolia.fi/~tomiin/nightlife/?api&getimages&barid=" + id + "&limit=" + limit + "&offset" + offset + "&json");
            httpURLConnection = (HttpURLConnection) url.openConnection();
            final String str = InputStreamToString(httpURLConnection.getInputStream());
            System.out.println("str!!!!" + str);
            JSONArray jsonArray = new JSONArray(str);
            final int length = jsonArray.length();

            List<Bitmap> list = new ArrayList<>(length);
            for (int i = 0; i < length; i++) {
                final JSONObject jsonObject = jsonArray.getJSONObject(i);
                final String data = jsonObject.getString("data");
                final Bitmap bitmap = decodeBase64(data);
                if (bitmap != null) {
                    list.add(decodeBase64(data));
                }
            }

            //final JSONArray jsonArray = new JSONArray(str);
            return list;
            //final Object object=jsonArray.get(0);

        } catch (final Exception ex) {
            ex.printStackTrace();
            return null;
        } finally {
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }
    }

    private static String InputStreamToString(InputStream is) throws IOException {
        final BufferedReader br = new BufferedReader(new InputStreamReader(is));
        final StringBuilder sb = new StringBuilder();
        try {
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } finally {
            try {
                br.close();
            } catch (IOException e) {
            }
        }

        return sb.toString();
    }


    private static final ExecutorService INCREMENT_CLICK_EXCECUTOR = Executors.newSingleThreadExecutor();

    public static final boolean incrementClickCountOnThread(final long barId, final String userId) {
        final Future<Boolean> future = INCREMENT_CLICK_EXCECUTOR.submit(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return incrementClickCount(barId, userId);
            }
        });

        try {
            return future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return false;
    }

    private static final boolean incrementClickCount(final long barId, final String userId) {
        boolean ret = false;
        HttpURLConnection httpURLConnection = null;
        try {
            final URL url = new URL("http://users.metropolia.fi/~tomiin/nightlife/?api&addclick&barid=" + barId + "&userid=" + userId + "&json");

            System.out.println("http://users.metropolia.fi/~tomiin/nightlife/?api&addclick&barid=" + barId + "&userid=" + userId + "&json");
            httpURLConnection = (HttpURLConnection) url.openConnection();
            final String str = InputStreamToString(httpURLConnection.getInputStream());
            System.out.println("TAGGGGG" + str);
            //final JSONArray jsonArray = new JSONArray(str);
            ret = Boolean.valueOf(str);
            System.out.println("TAGGGGG" + ret);
            //final Object object=jsonArray.get(0);

        } catch (final Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }
        return ret;
    }

    public static String toString(Object object) {
        final StringBuilder stringBuilder = new StringBuilder();

        final Field[] fields = object.getClass().getDeclaredFields();
        for (int i = 0, length = fields.length; i < length; i++) {
            final Field field = fields[i];
            stringBuilder.append(field.getName());
            stringBuilder.append('=');
            try {
                stringBuilder.append(field.get(object));
            } catch (IllegalAccessException | IllegalArgumentException e) {
                stringBuilder.append("IllegalAccessOrArgument");
            }
            stringBuilder.append(',');
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    public static class InterruptTimerTask extends TimerTask {
        private final Thread thread;

        public InterruptTimerTask(Thread thread) {
            this.thread = thread;
        }

        public void run() {
            thread.interrupt();
            cancel();
        }
    }
}